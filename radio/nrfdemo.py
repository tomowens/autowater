
import time
import struct
import pigpio
from nrf24 import *
from zones import Zone

pig_name = 'zero.ourhouse'
pig_port = 8888
address = ['1SRVR', '1CLNT']

print("Connecting to piGPIO on {} ...".format(pig_name))
pi = pigpio.pi(pig_name, pig_port)
if not pi.connected:
    print("Unable to connect to {} ... ending".format(pig_name))
    sys.exit(1)

nrf = NRF24(pi, ce=25, payload_size=RF24_PAYLOAD.DYNAMIC, channel=100, data_rate=RF24_DATA_RATE.RATE_250KBPS, pa_level=RF24_PA.LOW)
nrf.set_address_bytes(len(address[0]))
nrf.set_retransmission(15,15)
nrf.open_reading_pipe(RF24_RX_ADDR.P1, address[0])
nrf.open_writing_pipe(address[1])
nrf.show_registers()

recv_queue = []

def get_first_message():
    if len(recv_queue) == 0:
        return None
    return recv_queue.pop(0)

def request_water(zone):
    cmd = 'W'
    payload = struct.pack('<Bh', ord(cmd),  zone)
    send_payload(payload)
    

def set_zone_config(zone_num):
    cmd = 'C'
    zone = Zone.get_zone(zone_num)
    payload = struct.pack('<BHHBB??', ord(cmd),
                           zone.threshold, zone.last_reading,
                           zone.duration, zone.num,
                           zone.active, zone.degraded)
    send_payload(payload) 

def run():
    while True:
        try:
            while nrf.data_ready():
                payload = nrf.get_payload()
                hex = ':'.join(f'{i:02x}' for i in payload)
                print("payload size: {}".format(len(payload)))
                print("payload: {}".format(hex))
                recv_queue.append(payload)
                # proc_payload(payload)
                time.sleep(0.1)
        except:
            nrf.power_down()
            pi.stop()
    nrf.power_down()
    pi.stop()

def send_payload(payload):
    nrf.reset_packages_lost()
    nrf.send(payload)
    timeout = False
    nrf.wait_until_sent()
        
    lost = nrf.get_packages_lost()
    if lost:
        print("Timeout. No ack received.")

def proc_payload(data):
    msg_type = data[0]
    print("Received msg_type {}".format(msg_type))
    if msg_type == 1:
        zone = Zone.get_zone(data[6])
        zone.recv_poll_data(data)
        print("Zone {} moisture {}".format(zone.num,zone.last_reading))
        print("-------")

if __name__ == '__main__':
    # stupid hack for a sibling import
    from sys import path
    from os.path import dirname as dir

    path.append(dir(path[0]))
    __package__ = "autowater"
    from zones import Zone

    run()