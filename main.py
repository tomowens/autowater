#!/usr/bin/env python3

import time
import struct
import json
from threading import Thread
import paho.mqtt.client as mqtt
from radio import nrfdemo as nrf
from zones import Zone

NUM_ZONES = 4
client = mqtt.Client(client_id='autowater')

def run_nrf():
    nrf.run()

def zone_mon():
    counter = 0
    while counter <= 73:
        msg = nrf.get_first_message()
        if msg:
            print("Message {} is {} bytes | {}".format(counter, len(msg), msg))
            proc_message(msg)
            counter += 1
        time.sleep(2)

def proc_message(msg):
    if msg[0] == 1:    # zone status message
        data = struct.unpack('<BHHBB??', msg)
        zone = Zone.get_zone(data[4])
        zone.recv_poll_data(data)
        zone.print_status()
        mqtt_send(zone)
    elif msg[0] == 2 or msg[0] == 3:  # watering message
        data = struct.unpack('<BH?', msg)
        zone = Zone.get_zone(data[1])
        if msg[0] == 2:
            action = 'watering'
            zone.watering = bool(msg[3])
            mqtt_send(zone)
        else:
            action = 'degraded mode'
        print("Zone {} {}: {}".format(zone.num, action, data[2]))
    elif msg[0] == 4:
        data = struct.unpack('<Bff', msg)
        mqtt_send_climate(data)
        print("Temp C: {} Humidity %: {}".format(data[2], data[1]))
    else:
        print("####### Unknown message type received. #######")

def water_request(msg):
    topic = msg.topic.rstrip('/')
    zone = int(topic[-1])
    print("Requesting water for zone {}".format(zone))
    nrf.request_water(zone)

def config_request(msg):
    topic = msg.topic.rstrip('/')
    zone = Zone.get_zone(int(topic[-1]))
    z_dict = json.loads(msg.payload.decode('utf-8'))
    print("z_dict: {}".format(z_dict))
    zone.threshold = int(z_dict['threshold'])
    zone.duration = int(z_dict['duration'])
    zone.active = bool(z_dict['active'])
    print(zone.__dict__)
    nrf.set_zone_config(zone.num)

def mqtt_recv(client, userdata, message):
    topic = message.topic
    print("mqtt message: {} | {}".format(message.topic, message.payload))
    if 'request' in topic:
        water_request(message)
    elif 'configure' in topic:
        config_request(message)

def mqtt_send(zone):
    topic = 'autowater/zone/status/{}'.format(zone.num)
    message = json.dumps(zone.__dict__)
    print("mqtt_send: {}".format(message))
    client.publish(topic, payload=message)

def mqtt_send_climate(data):
    readings = {}
    readings['tempc'] = round(data[2],2)
    readings['tempf'] = round((data[2] * 9.0/5.0)+32, 2)
    readings['humidity'] = data[1]
    topic = 'autowater/climate/status'
    message = json.dumps(readings)
    print("mqtt_send: {}".format(message))
    client.publish(topic, payload=message)
    
def setup_mqtt():
    client.on_message = mqtt_recv
    client.connect('zero.ourhouse')
    client.subscribe('autowater/#')
    client.loop_start()

def run():
    for zone in range(NUM_ZONES):
        Zone.get_zone(zone)
    nrf = Thread(target=run_nrf, daemon=True)
    nrf.start()
    setup_mqtt()
    zone_mon()
    

if __name__ == '__main__':
    run()
    