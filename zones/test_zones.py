import unittest
from unittest import mock

from zones import Zone

class TestZones(unittest.TestCase):

    def setUp(self):
        self.zone = Zone
    
    def test_zone_init(self):
        mock_num = 1
        z = self.zone(mock_num)
        assert z.num == mock_num
        assert z.threshold == 512
        assert z.duration == 30
        assert z.last_reading == 0
        assert z.active == True
        assert z.degraded == False

    def test_get_zones(self):
        z = self.zone.get_zones()
        assert 0 <= len(z)

    def test_create_zone(self):
        mock_num = 2
        z = self.zone.create_zone(mock_num)
        assert z.num == mock_num
        assert len(Zone.get_zones()) > 0
