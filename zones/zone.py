"""The Zone class."""

class Zone():
    
    zones = []

    def __init__(self, num=None):
        self.num = num
        self.threshold = 512
        self.duration = 30
        self.last_reading = 0
        self.active = True
        self.degraded = False
        self.watering = False

    
    @classmethod
    def get_zones(cls):
        return cls.zones
    
    @classmethod
    def get_zone(cls, zone_num):
        for z in cls.zones:
            if zone_num == z.num:
                print("Found zone number {}".format(zone_num))
                zone = z
                break
        else:
            print("Creating new zone number {}".format(zone_num))
            zone = cls.create_zone(zone_num)

        return zone

    @classmethod
    def create_zone(cls, num):
        zone = Zone(num)
        cls.zones.append(zone)
        return zone

    def recv_poll_data(self, data):
        print("Received {} data fields".format(len(data)))
        # d = struct.unpack('<BHHBB??', data)
        # print(d)
        self.threshold = data[1]
        self.last_reading = data[2]
        self.duration = data[3]
        self.active = data[5]
        self.degraded = data[6]

    def print_status(self):
        print("Zone {} : threshold {} | reading {} | duration {} | active {} | degraded {}".format(
        self.num, self.threshold, self.last_reading, self.duration, self.active, self.degraded))