#include <RF24.h>
#include <DHT.h>
#include "zone.h"

#define PIN_RF24_CSN             10                // CSN PIN for RF24 module.
#define PIN_RF24_CE              9                 // CE PIN for RF24 module.
#define NRF24_CHANNEL            100               // 0 ... 125
#define NRF24_CRC_LENGTH         RF24_CRC_16       // RF24_CRC_DISABLED, RF24_CRC_8, RF24_CRC_16 for 16-bit
#define NRF24_DATA_RATE          RF24_250KBPS      // RF24_2MBPS, RF24_1MBPS, RF24_250KBPS
#define NRF24_DYNAMIC_PAYLOAD    1
#define NRF24_PAYLOAD_SIZE       32                // Max. 32 bytes.
#define NRF24_PA_LEVEL           RF24_PA_HIGH      // RF24_PA_MIN, RF24_PA_LOW, RF24_PA_HIGH, RF24_PA_MAX    
#define NRF24_RETRY_DELAY        10                // Delay bewteen retries, 1..15.  Multiples of 250µs.
#define NRF24_RETRY_COUNT        15                // Number of retries, 1..15.
#define DHT_PIN                  6                 // Pin for DHT11 sensor
#define DHT_TYPE                 DHT11             // DHT sensor type

#define NUM_ZONES                4
#define ZONE_POLL_TIME           60                // Delay seconds between moisture polling intervals
#define DEGRADED_HOURS           8                 // Default watering interval if sensor fails
#define HIGH_LIMIT               950               // Sensor failure threshold
#define LOW_LIMIT                75                // Sensor failuire threshold

const int analog_pins[NUM_ZONES] = {A0, A1, A2, A3};
// Adjust this as needed for board type
const int digital_pins[NUM_ZONES] = {2, 3, 4, 5};

byte rf24_tx[6] = "1SRVR";                        // Address used when transmitting data.
byte rf24_rx[6] = "1CLNT";                        // Address used when receiving data.
byte payload[32];                                 // Payload bytes. Used both for transmitting and receiving

RF24 radio(PIN_RF24_CE, PIN_RF24_CSN);            // Instance of RF24.

byte zones_to_water = 0;
byte watering_zone  = 0x0F;

// DHT11 temperature / humidity sensor
DHT dht(DHT_PIN, DHT_TYPE);
float dht_readings[2];

// various timers
unsigned long last_poll_time = 0;
unsigned long water_start    = 0;
unsigned long last_degrade   = 0;
unsigned long poll_delay     = (unsigned long)ZONE_POLL_TIME * 1000;
unsigned long degrade_delay  = (unsigned long)DEGRADED_HOURS * 60 * 60 * 1000;

void setup() {
  // put your setup code here, to run once:
  for (int i = 0; i < NUM_ZONES; i++ ) {
    pinMode(analog_pins[i], INPUT_PULLUP);
    pinMode(digital_pins[i], OUTPUT);
    digitalWrite(digital_pins[i], HIGH);
    Serial.begin(115200);
    dht.begin();
    delay(100);

    // Configure the NRF24 tranceiver.
    Serial.println("Configure NRF24 ...");
    nrf24_setup();
  }
}

void loop() {
  // put your main code here, to run repeatedly:
  poll_dht11();
  poll_sensors();
  check_watering();
  check_cmd();
  water_degraded();
  delay(1000);
}

void check_cmd() {
  int payload_size = 0;
  if (radio.available()) {
    Serial.println(F("Cmd received"));
    payload_size = radio.getDynamicPayloadSize();
    radio.read(&payload, payload_size);
    Serial.print(F("Payload size : "));
    Serial.print(payload_size);
    Serial.println(F(" bytes"));
    radio.flush_rx();
    delay(100);
  }
  if (payload[0] == 'C') {
    byte z = payload[6];
    byte dur = payload[5];
    bool active = payload[7];
    int ref = (payload[1] + (payload[2] << 8));
    Serial.print(F("Setting zone "));
    Serial.print(z);
    Serial.print(F(" reference: "));
    Serial.print(ref);
    Serial.print(F(" duration: "));
    Serial.print(dur);
    Serial.print(F(" active: "));
    Serial.println(active);

    zones[z].reference = ref;
    zones[z].duration = dur;
    zones[z].active = active;
    payload[0] = 0x00;
  }

  if (payload[0] == 'W') {
    byte z = payload[1];
    if (z < NUM_ZONES) {
      Serial.print(F("Requesting water for zone "));
      Serial.println(z);
      zones_to_water |= (1 << z);
    }
    else {
      Serial.print(F("Unknown zone : "));
      Serial.println(z);
    }
    payload[0] = 0x00;
  }
  return;
}

void poll_sensors() {
  unsigned long current = millis();
  if ((current - last_poll_time) >= poll_delay) {
    for (int i = 0; i < NUM_ZONES; i++) {
      process_zone(i);
      delay(100);
    }
    last_poll_time = millis();
  }
  return;
}

void poll_dht11() {
  unsigned long current = millis();
  if ((current - last_poll_time) >= poll_delay) {
    dht_readings[0] = dht.readHumidity();
    dht_readings[1] = dht.readTemperature();
    send_dht11();
  }
  return;
}

void process_zone(int zone_num) {
  int reading = (1024 - analogRead(analog_pins[zone_num]));
  zones[zone_num].current = reading;
  zones[zone_num].num = zone_num;

  send_zone(zone_num, &zones[zone_num]);

  if (reading >= HIGH_LIMIT || reading <= LOW_LIMIT) {
    if (zones[zone_num].degraded == false) {
      Serial.print(F("Zone "));
      Serial.print(zone_num);
      Serial.println(F(" switching to degraded mode"));
      send_degraded(zone_num, true);
    }
    zones[zone_num].degraded = true;
    return;
  }
  else {
    if (zones[zone_num].degraded) {
      Serial.print(F("Zone "));
      Serial.print(zone_num);
      Serial.println(F(" return from degraded mode")); 
      zones[zone_num].degraded = false;
      send_degraded(zone_num, false);
    }
  }

  if (reading <= zones[zone_num].reference && zones[zone_num].active) {
    Serial.print(F("Requesting water zone: "));
    Serial.print(zone_num);
    Serial.print(F(" | reading: "));
    Serial.println(reading);
    
    zones_to_water |= (1 << zone_num);
  }
}

void check_watering() {
  if (! zones_to_water) {
    return;
  }

  if (watering_zone == 0x0F) {        // not currently watering
    Serial.print(F("zones_to_water : "));
    Serial.println(zones_to_water);

    for (int i = 0; i < NUM_ZONES; i++) {
      if (zones_to_water & (1 << i)) {
        Serial.print(F("Starting water for zone "));
        Serial.println(i);
        watering_zone = (byte) (i & 0xff);
        water_start = millis();
        digitalWrite(digital_pins[i], LOW);
        send_water(i, true);
        break;
      }
    }
  }
  else {
    if ((millis() - water_start) < (unsigned long) (zones[watering_zone].duration) * 1000) {
      return;
    }
    Serial.print(F("Finished water for zone "));
    Serial.println(watering_zone);
    digitalWrite(digital_pins[watering_zone], HIGH);
    zones_to_water -= (1 << watering_zone);
    send_water(watering_zone, false);
    watering_zone = 0x0F;
  }
}

void water_degraded() {
  if (millis() - last_degrade > degrade_delay) {
    for (int i = 0; i < NUM_ZONES; i++) {
      if (zones[i].degraded) {
        zones_to_water |= (1 << i);
      }
    }
    last_degrade = millis();
  }
}

void send_zone(int num, Zone* zone) {
  int offset = 0;
  char msg_type = 1;
  memcpy(payload + offset, (char *)&msg_type, sizeof(msg_type)); offset += sizeof(msg_type);
  memcpy(payload + offset, zone, sizeof(Zone)); offset += sizeof(Zone);
  nrf_send(offset);
}

void send_water(int num, bool state) {
  int offset = 0;
  char msg_type = 2;
  memcpy(payload + offset, (char *)&msg_type, sizeof(msg_type)); offset += sizeof(msg_type);
  memcpy(payload + offset, (int *)&num, sizeof(num)); offset += sizeof(num);
  memcpy(payload + offset, (bool *)&state, sizeof(state)); offset += sizeof(state);
  nrf_send(offset);
}

void send_degraded(int num, bool state) {
  int offset = 0;
  char msg_type = 3;
  memcpy(payload + offset, (char *)&msg_type, sizeof(msg_type)); offset += sizeof(msg_type);
  memcpy(payload + offset, (int *)&num, sizeof(num)); offset += sizeof(num);
  memcpy(payload + offset, (bool *)&state, sizeof(state)); offset += sizeof(state);
  nrf_send(offset);
}

void send_dht11() {
  int offset = 0;
  char msg_type = 4;
  memcpy(payload + offset, (char *)&msg_type, sizeof(msg_type)); offset += sizeof(msg_type);
  memcpy(payload + offset, (int *)&dht_readings, sizeof(dht_readings)); offset += sizeof(dht_readings);
  nrf_send(offset);
}

void nrf_send(int num_bytes) {
  // prepare to transmit
  radio.stopListening();
  radio.flush_tx();

  Serial.println(F("Sending message to nrf24"));
  if (radio.write(payload, num_bytes)) {
      // Request was successful.
      Serial.print("Success. Retries="); Serial.println(radio.getARC());
  }
  else {
      Serial.print(">>> ERROR. Retries="); Serial.println(radio.getARC());
  }
  radio.startListening();
}

void nrf24_setup()
{
  radio.begin();
  radio.enableDynamicPayloads();
  radio.setAutoAck(true);                 
  radio.setPALevel(NRF24_PA_LEVEL);
  radio.setRetries(NRF24_RETRY_DELAY, NRF24_RETRY_COUNT);              
  radio.setDataRate(NRF24_DATA_RATE);          
  radio.setChannel(NRF24_CHANNEL);
  radio.setCRCLength(NRF24_CRC_LENGTH);
  radio.setPayloadSize(NRF24_PAYLOAD_SIZE);
  radio.openWritingPipe(rf24_tx);
  radio.openReadingPipe(1, rf24_rx);
  radio.startListening();
}